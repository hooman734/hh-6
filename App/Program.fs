﻿open System
open System.Net
open System.IO

[<EntryPoint>]
let main argv =
    
    let content1 = File.ReadAllText("sample1.txt")
    let content2 = File.ReadAllText("sample2.txt")
    let content = content1.Split "\n"
    content.
   
    File.WriteAllText("output.txt", content1+Environment.NewLine+content2)
    
    printfn "%A" argv
    0 // return an integer exit code

let textJoin (s1: string,  s2: string) =
    s1